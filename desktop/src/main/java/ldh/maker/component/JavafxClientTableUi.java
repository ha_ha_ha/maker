package ldh.maker.component;

import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class JavafxClientTableUi extends PojoTableUi {

    public JavafxClientTableUi(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

}
