package db;

import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.EnumStatusMaker;
import ldh.maker.util.ConnectionFactory;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.EnumData;

import java.sql.Connection;
import java.util.List;

/**
 * Created by ldh on 2017/4/7.
 */
public class FunctionTest {

    public static void main(String[] args) {
        DBConnectionData data = new DBConnectionData();
        data.setUserNameProperty("root");
        data.setPasswordProperty("123456");
        data.setIpProperty("localhost");
        data.setPortProperty(3306);
        Connection connection = ConnectionFactory.getConnection(data);
        TableInfo tableInfo = new TableInfo(connection, "maker", null, true);
        Table table =tableInfo.getTable("user");
        List<Column> columns = table.getColumnList();
        columns.forEach(c->System.out.println("\tcolumns:" + c.getName()));
//        FreeMakerUtil.getAllFunction(tableInfo.getTable(""));

        String pojoPackage = "ldh.caikee.test";
        int t = pojoPackage.lastIndexOf(".");
        String s = pojoPackage.substring(0, t-1);
        System.out.println("sadfsa:" + s);

        EnumStatusMaker enumStatusMaker = new EnumStatusMaker();
        enumStatusMaker.pack("ldh.school.constant")
                .name("StudentSexEnum")
                .type(Boolean.class)
                .addValue("Online", new EnumData(true, "男性"))
                .addValue("Offline", new EnumData(false, "女性"));
        String code = enumStatusMaker.toString();
        System.out.println(code);

    }
}
