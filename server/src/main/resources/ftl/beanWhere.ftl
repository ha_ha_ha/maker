package ${package};

/**
 * @Auther: ${Author}
 * @Date: ${DATE}
<#if table.comment??>
 * @Description: ${table.comment}
</#if>
 */

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

<#if serializable>@SuppressWarnings("serial")</#if>
public class ${className} <#if extend?exists>extends ${extend}</#if> <#if implements?exists>implements <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if>${r'{'}

	private long start;
	private int length = 10;
	private long pageNo;
	private int pageSize = length;
	private long end = start + pageSize;
	private String order;
	
	<#if table.columnList??>
	<#list table.columnList as column>
	<#if column.javaType == 'Date'>
	/**
	 * 开始时间： ${util.comment(column)}
	 */
	private Date start${util.firstUpper(column.property)};
	
	/**
	 * 结束时间： ${util.comment(column)}
	 */
	private Date end${util.firstUpper(column.property)};
	
	</#if>
	</#list>
	</#if>
	
	@Override
	public long getStart() {
		return start;
	}
	
	@Override
	public int getLength() {
		return length;
	}
	
	@Override
	public long getEnd() {
		return end;
	}

	@Override
	public long getPageNo() {
		return pageNo;
	}

    @Override
	public void setPageNo(long pageNo) {
		this.pageNo = pageNo;
		setPageSize(this.pageSize);
	}

	@Override
	public int getPageSize() {
		return pageSize;
	}

	@Override
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
		start = (pageNo - 1) * pageSize;
		if (start < 0) start = 0;
		length = pageSize;
		end = start + pageSize;
	}

	@Override
	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String getOrder() {
		return order;
	}
	
	<#if table.columnList??>
	<#list table.columnList as column>
	<#if column.javaType == 'Date'>
	/**
	 * 设置开始时间： ${util.comment(column)}
	 */
	public void setStart${util.firstUpper(column.property)}(Date start${util.firstUpper(column.property)}) {
		this.start${util.firstUpper(column.property)} = start${util.firstUpper(column.property)};
	}
	
	/**
	 * 获取开始时间： ${util.comment(column)}
	 */
	public Date getStart${util.firstUpper(column.property)}() {
		return this.start${util.firstUpper(column.property)};
	}
	
	/**
	 * 设置结束时间： ${util.comment(column)}
	 */
	public void setEnd${util.firstUpper(column.property)}(Date end${util.firstUpper(column.property)}) {
		this.end${util.firstUpper(column.property)} = end${util.firstUpper(column.property)};
	}
	
	/**
	 * 获取结束时间： ${util.comment(column)}
	 */
	public Date getEnd${util.firstUpper(column.property)}() {
		return this.end${util.firstUpper(column.property)};
	}
	</#if>
	</#list>
	</#if>
${r'}'}

