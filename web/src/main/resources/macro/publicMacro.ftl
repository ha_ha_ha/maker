<#--公共头部-->
<#macro header title keywords="" description="">
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; shrink-to-fit=no; maximum-scale=1.0;" />
    <meta name="format-detection" content="telephone=no" />
    <title>${title}</title>
    <meta name="keywords" content="${keywords}" />
    <meta name="description" content="${description}" />

    <link href="/resource/frame/bootstrap3/css/bootstrap.css" rel="stylesheet">
    <link href="/resource/frame/bootstrap3/css/dashboard.css" rel="stylesheet">

    <#nested>

</head>
<body>
</#macro>

<#macro body>
    <#include "/common/head.ftl"/>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <#include "/common/left.ftl"/>
            </div>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <#nested>
            </div>
        </div>
    </div>
</#macro>
<#--公共底部-->
<#macro footer>
    <script src="/resource/common/js/jquery-1.8.0.min.js"></script>
    <script src="/resource/frame/bootstrap3/js/bootstrap.js"></script>
    <#nested>
</body>
</html>
</#macro>