<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib  prefix="Template" tagdir="/WEB-INF/tags" %>

<Template:main>
    <jsp:body>
        <h2>${util.comment(table)}详情</h2>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <th>名称</th>
                    <th>值</th>
                </thead>
                <tbody>
                <#list table.columnList as column>
                    <tr>
                        <th>${util.comment(column)}</th>
                        <#if column.foreign>
                        <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'}'}</th>
                        <#elseif util.isDate(column)>
                        <th><fmt:formatDate value="${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}" pattern="yyyy-MM-dd HH:mm:ss"/> </th>
                        <#elseif util.isNumber(column)>
                        <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}</th>
                        <#elseif util.isEnum(column)>
                        <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}</th>
                        <#else>
                        <th>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'}'}</th>
                        </#if>
                    </tr>
                </#list>
                </tbody>
            </table>
        </div>
    </jsp:body>
</Template:main>