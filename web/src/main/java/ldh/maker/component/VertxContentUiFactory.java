package ldh.maker.component;

public class VertxContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new VertxContentUi();
    }
}
