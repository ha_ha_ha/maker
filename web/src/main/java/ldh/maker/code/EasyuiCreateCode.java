package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.freemaker.PomXmlMaker;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ldh on 2017/4/6.
 */
public class EasyuiCreateCode extends WebCreateCode {

    public EasyuiCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
        jspFtls.put("/easyui/jspList.ftl","_all");
        jsFtls.add("/easyui/jsList.ftl");
        add("/easyui/jspMain.ftl", "main.jsp", "jsp");
        addData("controllerFtl", "/easyui/controller.ftl");
    }

    @Override
    protected void createOnce(){
        super.createOnce();

        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "main", "webapp", "resource"));
        try {
            copyResources("common/easyui", dirs, "common");
            copyResources("common/js", dirs, "common", "js");
            copyResources("frame", dirs, "frame");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void buildPomXmlMaker(String path) {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .ftl("/easyui/pom.ftl")
                .outPath(resourcePath)
                .make();
    }

    public String getProjectName() {
        return data.getProjectNameProperty();
    }
}
