import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../loading-list.dart';

class ${util.firstUpper(table.javaName)}ListPage extends LoadingListPage {
  ${util.firstUpper(table.javaName)}ListPage({Key key, String title}) : super(key: key, title: title);

  @override
  void loadData() {
    _load();
  }

  bool loading = false;
  int page = 1;
  Future<Null> _load() async {
    if (loading) {
      return null;
    }
    loading = true;
    try {
      var url = "https://api.github.com/repositories/31792824/issues?page=$page";
      print("url: $url");
      var resp = await http.get(url);
      var data = json.decode(resp.body);
      page += 1;
      List list = new List();
      if (data is List) {
        data.forEach((dynamic e) {
          if (e is Map) {
            list.add(e['title'] as String);
          }
        });
      }
      setData(list);
    } finally {
      loading = false;
    }
  }

  @override
  Widget buildListCell(BuildContext context, title) {
     return ListTile(
        key: new ValueKey<String>(title),
        title: new Text(title)
    );
  }
}


