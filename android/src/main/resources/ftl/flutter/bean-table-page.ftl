import 'package:flutter/material.dart';
<#list tableInfo.tables?keys as key>
  <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
  import "modules/${util.dartName(tableInfo.tables[key].javaName)}-table.dart";
  </#if>
</#list>

class BeanTablePage extends StatelessWidget {
  const BeanTablePage({Key key, this.name, this.title}) : super(key : key);

  final String name;
  final String title;

  @override
  Widget build(BuildContext context) {
    <#list tableInfo.tables?keys as key>
      <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
      if (name == "${tableInfo.tables[key].javaName}") {
        return ${util.firstUpper(tableInfo.tables[key].javaName)}TablePage(name: name, title: title);
      }
      </#if>
    </#list>

    return null;
  }

}