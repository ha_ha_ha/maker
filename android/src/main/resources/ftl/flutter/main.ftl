import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'dart:ui';//window.physicalSize.height
import 'package:flutter/rendering.dart' show debugPaintSizeEnabled;
import "pages/home-page.dart";
import "pages/welcome-page.dart";
<#list tableInfo.tables?keys as key>
  <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
import "pages/modules/${util.dartName(tableInfo.tables[key].javaName)}-list.dart";
  </#if>
</#list>

void main() {
//  debugPaintSizeEnabled = true; //打开视觉调试开关
  runApp(new MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new _MainPage(),
      routes: <String, WidgetBuilder>{
      <#list tableInfo.tables?keys as key>
        <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
        "/${util.firstLower(tableInfo.tables[key].javaName)}": (context) => new ${util.firstUpper(tableInfo.tables[key].javaName)}ListPage(),
        </#if>
      </#list>
      },
    );
  }
}

class _MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<_MainPage> {

  bool isGuide = false;
  @override
  Widget build(BuildContext context) {
    print("is guide $isGuide");
    if (isGuide) {
      return HomePage();
    }
    return WelcomePage(callback: (){
      setState(() {
        isGuide = true;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _isGuideCheck();
  }

  _isGuideCheck() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isGuide = await prefs.getBool('isGuide')?? false;
    print('Pressed $isGuide times.');
    setState(() {
      isGuide = true;
    });
  }
}






