import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart'as http;
import '../loading-table.dart';

class ${util.firstUpper(table.javaName)}TablePage extends LoadingTable<${util.firstUpper(table.javaName)}> {

  ${util.firstUpper(table.javaName)}TablePage({Key key, String name, String title}) : super(key : key, name: name, title: title);

  @override
  void loadingData() {
     new Future.delayed(new Duration(seconds: 2), () {
        setData(new List());
    });
  }

  @override
  List<SortDataColumn> sortDataColumns() {
    return <SortDataColumn>[
    <#if table.columnList??>
        <#list table.columnList as column>
        new SortDataColumn.sort(
            label: const Text('${util.comment(column)}'),
            <#if util.isNumber(column)>
            numeric: true,
            </#if>
        ),
        </#list>
    </#if>
    ];
  }

  @override
  List<Widget> celles(${util.firstUpper(table.javaName)} ${util.firstLower(table.javaName)}) {
    return <Widget>[
    <#if table.columnList??>
        <#list table.columnList as column>
     new Text('${r'${'}${util.firstLower(table.javaName)}.${column.property}}'),
        </#list>
    </#if>
    ];
  }
}

class ${util.firstUpper(table.javaName)} {
  ${util.firstUpper(table.javaName)}(${util.dartBeanParams(table)});

  <#if table.columnList??>
    <#list table.columnList as column>
    final ${util.dartType(column)} ${column.property};
    </#list>
  </#if>

  bool selected = false;
}