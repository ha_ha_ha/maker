import 'package:flutter/material.dart';
import 'data-table.dart';
import 'bean-table-page.dart';

final List<_Page> _pages = [
  new _Page(icon: new Icon(Icons.ac_unit), title: "主界面"),
  new _Page(icon: new Icon(Icons.access_alarms), title: "监控页面"),
  new _Page(icon: new Icon(Icons.accessible_forward), title: "报警页面"),
  new _Page(icon: new Icon(Icons.account_box), title: "消息页面"),
];

final List<_Page> _tableIcons = [
  <#list tableInfo.tables?keys as key>
    <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
  new _Page(icon: new Icon(Icons.${icons[key_index]}, size: 56.0, color: Colors.blueAccent), title: "${util.comment(tableInfo.tables[key])}管理", name: "${tableInfo.tables[key].javaName}"),
    </#if>
  </#list>
];

class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {

  int _currentPageIndex = 0;
  PageController _pageController = new PageController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("代码自动生成flutter"),
        actions: <Widget>[
          new PopupMenuButton(itemBuilder: (build) {
            return [new PopupMenuItem(child: new Text("exit!"))];
          })
        ],
      ),
      body: new PageView.builder(
          itemCount: _pages.length,
          controller: _pageController,
          itemBuilder: (BuildContext context, int index) {
            return new _Body(index: index);
          },
          onPageChanged: (int value){
            _pageChange(value);
          }
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            userHeader,
            Expanded(
              child: MediaQuery.removePadding(
               context: context,
               removeTop: true,
               child: ListView(
                children: _tableIcons.map((page) {
                  return Column(
                    children: <Widget>[
                      ListTile(
                        title: new Text(page.name),
                        subtitle: new Text(page.title),
                        leading: page.icon,
                        onTap: (){
                          Navigator.of(context).pushNamed("/${r'${'}page.name}");
                        },
                      ),
                      Divider(),
                    ],
                  );
                }).toList()
              ),
            ))
          ],
        )
      ),
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: _currentPageIndex,
        type: BottomNavigationBarType.shifting,
        onTap: (int index) {
          _pageController.animateToPage(
              index, duration: new Duration(seconds: 2),
              curve: new ElasticOutCurve(0.8));
          _pageChange(index);
        },
        items: _pages.map((_Page _page) {
          return new BottomNavigationBarItem(icon: _page.icon,
              title: new Text(_page.title),
              backgroundColor: Colors.blue);
        }).toList(),
      ),
    );
  }

  Widget userHeader = UserAccountsDrawerHeader(
    accountName: new Text('Tom'),
    accountEmail: new Text('tom@xxx.com'),
    currentAccountPicture: new CircleAvatar(
      backgroundImage: AssetImage('images/shuang.jpg'), radius: 35.0,
    ),
  );

  void _pageChange(int index) {
    setState(() {
      if (_currentPageIndex != index) {
        _currentPageIndex = index;
      }
    });
  }
}

class _Body extends StatelessWidget {
  const _Body({Key key, this.index}) : super(key: key);

  final int index;

  @override
  Widget build(BuildContext context) {
    if (index == 0) {
      return
        new GridView.count(
          crossAxisCount: 3,
          padding: EdgeInsets.only(left: 20.0, right: 5.0, top: 10.0, bottom: 5.0),
          mainAxisSpacing: 5.0,
          crossAxisSpacing: 10.0,
          children: _tableIcons.map((_Page _page) {
              return _BeanItem(icon: _page.icon, title: _page.title, name: _page.name);
          }).toList(),
        );
    } else if (index == 1) {
      return new Center(child: new Text("page 2"),);
    } else {
      return new Center(child: new Text("page $index"),);
    }
  }

}

class _BeanItem extends StatelessWidget {
  const _BeanItem({Key key, this.icon, this.title, this.name}) : super(key: key);
  final Icon icon;
  final String title;
  final String name;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      highlightColor: Colors.white30,
      onTap: () {
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) {
              return new BeanTablePage(name: name, title: name);
            })
        );
      },
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          icon,
          new Text(title,
            style: new TextStyle(
              color: Colors.lightBlueAccent,
              fontSize: 16.0,
            ),
            overflow: TextOverflow.clip,
            maxLines: 1,
          ),
        ],
      ),
    );
  }

}

class _Page {
  const _Page({this.icon, this.title, this.name});

  final Icon icon;
  final String title;
  final String name;
}