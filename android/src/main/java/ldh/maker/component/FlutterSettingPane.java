package ldh.maker.component;

import com.jfoenix.controls.JFXTabPane;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class FlutterSettingPane extends SettingPane {

    public FlutterSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

    @Override
    protected void initTab() {
        buildNoTablePaneTab();
        noTablePane.show();
    }

    @Override
    public boolean isSetting() {
        return true;
    }
}
