package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.KeyMaker;
import ldh.maker.freemaker.SimpleJavaMaker;
import ldh.maker.freemarker.FlutterMaker;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ldh123 on 2018/9/10.
 */
public class FlutterCreateCode extends CreateCode{

    public FlutterCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    @Override
    public void create() {
        if (!table.isCreate()) return;

        if (table.getPrimaryKey() != null && !table.getPrimaryKey().isComposite()) {
            Column c = table.getPrimaryKey().getColumns().iterator().next();
            key = c.getPropertyClass();
        }

        if (isCreate) {
            createOnce();
            isCreate = false;
        }

        new FlutterMaker()
                .fileName(FreeMakerUtil.dartName(table.getJavaName()) + "-page.dart")
                .ftl("/flutter/bean-table.ftl")
                .data("table", table)
                .outPath(createDartPath("lib", "pages", "modules"))
                .make();

        new FlutterMaker()
                .fileName(FreeMakerUtil.dartName(table.getJavaName()) + "-list.dart")
                .ftl("/flutter/bean-list.ftl")
                .data("table", table)
                .outPath(createDartPath("lib", "pages", "modules"))
                .make();
    }

    protected void createOnce() {
        createMain();

    }

    private void createMain() {
        String lib = createDartPath("lib");
        FlutterMaker flutterMaker = new FlutterMaker();

        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);

        flutterMaker.fileName("main.dart")
                .ftl("/flutter/main.ftl")
                .data("tableInfo", tableInfo)
                .outPath(lib)
                .data("title", "Flutter自动生成工具")
                .make();

        new FlutterMaker()
                .fileName("home-page.dart")
                .ftl("/flutter/home-page.ftl")
                .tableInfo(tableInfo)
                .outPath(createDartPath("lib", "pages"))
                .make();

        new FlutterMaker()
                .fileName("welcome-page.dart")
                .ftl("/flutter/welcome-page.ftl")
                .outPath(createDartPath("lib", "pages"))
                .make();

        new FlutterMaker()
                .fileName("window-util.dart")
                .ftl("/flutter/window-util.ftl")
                .outPath(createDartPath("lib", "util"))
                .make();

        new FlutterMaker()
                .fileName("loading-table.dart")
                .ftl("/flutter/loading-table.ftl")
                .data("tableInfo", tableInfo)
                .outPath(createDartPath("lib", "pages"))
                .make();

        new FlutterMaker()
                .fileName("loading-list.dart")
                .ftl("/flutter/loading-list.ftl")
                .data("tableInfo", tableInfo)
                .outPath(createDartPath("lib", "pages"))
                .make();

        new FlutterMaker()
                .fileName("bean-table-page.dart")
                .ftl("/flutter/bean-table-page.ftl")
                .data("tableInfo", tableInfo)
                .outPath(createDartPath("lib", "pages"))
                .make();
    }

    protected String createDartPath(String... dirs) {
        String path = FileUtil.getSourceRoot();
        List<String> dirst = new ArrayList<>(Arrays.asList("code", root, getProjectName()));
        Arrays.stream(dirs).forEach(e->dirst.add(e));
        path = makePath(path, dirst);
        return path;
    }

    public String getProjectName() {
        return "android-flutter";
    }
}
