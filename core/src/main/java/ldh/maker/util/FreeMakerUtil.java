package ldh.maker.util;

import ldh.bean.util.BeanInfoUtil;
import ldh.database.*;
import ldh.database.util.JdbcType;
import ldh.util.ConfigUtil;

import java.util.*;
import java.util.stream.Collectors;

public class FreeMakerUtil {

	public static String upFirst(String str) {
		if (str == null || str.trim().equals("")) return str;
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	public static String firstLower(String str) {
		if (str == null || str.trim().equals("")) return str;
		return str.substring(0, 1).toLowerCase() + str.substring(1);
	}

	public static String lowers(String str) {
		if (str == null || str.trim().equals("")) return str;
		return str.toLowerCase();
	}

	public static String insertColumn(Table table) {
		List<Column> columns = table.getColumnList();
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Column column : columns) {
			if (isFirst) {
				sb.append(column.getName());
				isFirst = false;
			} else {
				sb.append(",").append(column.getName());
			}

		}
		return sb.toString();
	}

	public static String insertValues(Table table) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		List<Column> columns = table.getColumnList();
		for (Column column : columns) {
			if(isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}
			sb.append("?");
		}
		return sb.toString();
	}

	public static String insertValue(Table table, Map<String, String> handlerMap) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		List<Column> columns = table.getColumnList();
		for (Column column : columns) {
			Class<?> ct = column.getPropertyClass();
			if (handlerMap != null && handlerMap.containsKey(ct.getName())) {
				if (isFirst) {
					sb.append("#{").append(column.getProperty()).append(", jdbcType=")
					  .append(column.getType())
					  .append("}");
					isFirst = false;
				} else {
					sb.append(",").append("#{").append(column.getProperty()).append(", jdbcType=")
					  .append(column.getType())
					  .append("}");
				}
			} else {
				if (isFirst) {
					sb.append("#{").append(column.getProperty()).append("}");
					isFirst = false;
				} else {
					sb.append(",").append("#{").append(column.getProperty()).append("}");
				}
			}


		}
		return sb.toString();
	}



	public static String insertSelective(Table table, Map<String, String> handlerMap) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		List<Column> columns = table.getColumnList();
		for (Column column : columns) {
			Class<?> ct = column.getPropertyClass();
			if (handlerMap != null && handlerMap.containsKey(ct.getName())) {
				if (isFirst) {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
							.append(column.getName()).append(" = ")
							.append("#{").append(column.getProperty()).append(", jdbcType=")
							.append(column.getType())
							.append("}")
							.append("</if>")
							.append("\n");
					isFirst = false;
				} else {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
							.append(", ").append(column.getName()).append(" = ")
							.append("#{").append(column.getProperty()).append(", jdbcType=")
							.append(column.getType())
							.append("}")
							.append("</if>")
							.append("\n");
				}
			} else {
				if (isFirst) {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
							.append(column.getName()).append(" = ")
							.append("#{").append(column.getProperty()).append("}")
							.append("</if>")
							.append("\n");
					isFirst = false;
				} else {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
							.append(", ").append(column.getName()).append(" = ")
							.append("#{").append(column.getProperty()).append("}")
							.append("</if>")
							.append("\n");
				}
			}
		}
		return sb.toString();
	}

	public static String update(Table table, Map<String, String> handlerMap) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		List<Column> columns = table.getColumnList();
		for (Column column : columns) {
			Class<?> ct = column.getPropertyClass();
			if (handlerMap != null && handlerMap.containsKey(ct.getName())) {
				if (isFirst) {
					sb.append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append(", jdbcType=")
					  .append(column.getType())
					  .append("}")
					  .append("\n");
					isFirst = false;
				} else {
					sb.append(", ").append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append(", jdbcType=")
					  .append(column.getType())
					  .append("}")
					  .append("\n");
				}
			} else {
				if (isFirst) {
					sb.append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append("}")
					  .append("\n");
					isFirst = false;
				} else {
					sb.append(", ").append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append("}")
					  .append("\n");
				}
			}
		}
		return sb.toString();
	}

	public static String updateNotnull(Table table, Map<String, String> handlerMap) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		List<Column> columns = table.getColumnList();
		for (Column column : columns) {
			Class<?> ct = column.getPropertyClass();
			if (handlerMap != null && handlerMap.containsKey(ct.getName())) {
				if (isFirst) {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
					  .append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append(", jdbcType=")
					  .append(column.getType())
					  .append("}")
					  .append("</if>")
					  .append("\n");
					isFirst = false;
				} else {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
					  .append(", ").append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append(", jdbcType=")
					  .append(column.getType())
					  .append("}")
					  .append("</if>")
					  .append("\n");
				}
			} else {
				if (isFirst) {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
				      .append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append("}")
					  .append("</if>")
					  .append("\n");
					isFirst = false;
				} else {
					sb.append("<if test=\"").append(column.getProperty()).append(" != null>")
				      .append(", ").append(column.getName()).append(" = ")
					  .append("#{").append(column.getProperty()).append("}")
					  .append("</if>")
					  .append("\n");
				}
			}
		}
		return sb.toString();
	}

	public static String leftJoin(Table table) {
		StringBuilder sb = new StringBuilder();
		List<Column> columns = table.getColumnList();
		for (Column column : columns) {
//			if (column.isForeign()) {
//				Table fn = column.getForeignTable();
//				sb.append("<#if column.property != null>")
//				  .append("left join ").append(fn.getName()).append(" as ").append(fn.getName())
//				  .append(" on fn.").append(column.getForeignKey()).append(" = ").append(column.getName())
//				  .append("<#if>")
//				  .append(" ");
//			}
		}
		return sb.toString();
	}

	public static boolean underLine(String str) {
		if (str == null || str.trim().equals("")) return false;
		int t = str.indexOf("_");
		if (t > -1) {
			return true;
		}
		return false;
	}

	public static Set<String> imports(Set<String> importList) {
		Set<String> set = new TreeSet<String>();
		for (String str : importList) {
			if (!str.startsWith("java.lang")) {
				set.add(str);
			}
		}
		return set;
	}

	public static String javaName(String name) {
		int t = name.indexOf("_");
		String str = name;
		if (t > -1) {
			String prefix = name.substring(0, t);
			if (t >= name.length()-1) {
				return prefix;
			}
			String suffix = name.substring(t+1);
			str = prefix + suffix.substring(0, 1).toUpperCase() + suffix.substring(1);
		}
		t = name.indexOf("_");
		if (t > -1) {
			return javaName(str);
		}
		return name.substring(0,1).toLowerCase() + name.substring(1);
	}

	public static String javaName(String table, String frefix) {
		if (frefix == null || frefix.trim().equals("")) {
			String t = table.toLowerCase();
			return javaName(t);
		} else {
			int t = table.toLowerCase().indexOf(frefix.toLowerCase());
			if (t == 0) {
				String ls = table.toLowerCase().substring(t + frefix.length());
				return javaName(ls);
			} else {
				return javaName(table);
			}
		}
	}

	public static String beanName(String table) {
		String str = javaName(table.toLowerCase());
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	public static String lower(String name) {
		return name.substring(0, 1).toLowerCase() + name.substring(1);
	}

	public static String firstUpper(String name) {
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	public static boolean isSerializable(Class<?> clazz) {
		return BeanInfoUtil.isSerializable(clazz);
	}

	public static String mapperKeyOrder() {
		String driver = ConfigUtil.getString("util.driverClassName");
		if(driver.contains("mysql")) {
			return "AFTER";
		} else if (driver.contains("oracle")) {
			return "BEFORE";
		} else {
			throw new RuntimeException("不支持这个数据库:" + driver);
		}
	}

	public static boolean isMysql() {
//		String driver = ConfigUtil.getString("util.driverClassName");
//		String driver = MakerConfig.getInstance().getParam().getDriverClassName();
//		if (driver.toLowerCase().contains("mysql")) {
//			return true;
//		}
		return true;
	}

	public static boolean isOracle() {
//		String driver = ConfigUtil.getString("util.driverClassName");
//		String driver = MakerConfig.getInstance().getParam().getDriverClassName();
//		if (driver.toLowerCase().contains("oracle")) {
//			return true;
//		}
		return false;
	}

	public static boolean containsKey(Map<String, String> handlerMap, Column column) {
		if (handlerMap == null) return false;
		Class<?> ct = column.getPropertyClass();
		return handlerMap.containsKey(ct.getName());
	}

	public static String comment(Column column) {
		if (column == null) return "";
		if (column.getText() != null) {
			return column.getText();
		}
		String comment = column.getComment();
		if (comment == null || comment.equals("")) {
			comment = column.getProperty();
		}
		if (comment.length() > 20) {
			comment = comment.substring(0, 20);
		}
		return comment;
	}

	public static String comment(Table table) {
		if (table == null) return "";
		String comment = table.getComment();
		if (comment == null || comment.equals("")) {
			comment = table.getJavaName();
		}
		if (comment.length() > 20) {
			comment = comment.substring(0, 20);
		}
		return comment;
	}

//	public static int[] rows(Table table) {
//		return rows(table.getColumnList());
//	}

	public static int[] rows(List<Column> columns) {
		int size = columns.size();
		for (Column c : columns) {
			if (c.getJavaType().endsWith("Date")) {
				size++;
			}
		}
		int row = size/4;
		if (row * 4 < size) {
			row++;
		}
		int [] rows = new int[row];
		for (int i=0; i<rows.length; i++) {
			rows[i] = 0;
		}
		return rows;
	}

//	public static Column column(Table table, int index) {
//		if (table.getColumnList().size() <= index) return null;
//		return table.getColumnList().get(index);
//	}

	public static Column column(List<Column> columns, int index) {
		if (columns.size() <= index) return null;
		return columns.get(index);
	}

	public static boolean hasDate(Table table) {
		for (Column column : table.getColumnList()) {
			if (column.getJavaType().endsWith("Date")) {
				return true;
			}
		}
		return false;
	}

	public static boolean isDate(Column column) {
		if (column.getJavaType().endsWith("Date")) {
			return true;
		}
		return false;
	}

	public static boolean isNumber(Column column) {
		if (column.getPropertyClass().isPrimitive() || (Number.class.isAssignableFrom(column.getPropertyClass()))) {
			return true;
		}
		return false;
	}

	public static boolean isJavafxProperty(Column column) {
		if (column.getJavaType().equals("Integer") ||
				column.getJavaType().equals("Long") ||
				column.getJavaType().equals("Double") ||
				column.getJavaType().equals("Float") ||
				column.getJavaType().equals("String") ||
				column.getJavaType().equals("Boolean")
				) {
			return true;
		}
		return false;
	}

	public static boolean isBigDecimal(Column column) {
		if (column.getJavaType().equals("BigDecimal")) {
			return true;
		}
		return false;
	}

	public static boolean isEnum(Column column) {
		if (column.getPropertyClass() == Enum.class) {
			return true;
		}
		return false;
	}

	public static boolean hasStatus(Table table) {
		for (Column column : table.getColumnList()) {
			if (column.getProperty().equalsIgnoreCase("status")) {
				return true;
			}
		}
		return false;
	}

	public static boolean contains(String str, String str1) {
		return str.contains(str1);
	}

	public static boolean isUnique(UniqueIndex ui, Table table) {
//		if (ui.getColumnNames().size() > 1) return true;
//		if (ui.getColumnNames().size() < 1) return false;
//		Column column = ui.getColumns().get(0);
////		for (ForeignKey k : table.getForeignKeys()) {
////			if (k.getColumn().getName().equals(column.getName())) {
////				return false;
////			}
////		}
//		if (column.getName().equals(table.getPrimaryKey().getColumn().getName())) {
//			return false;
//		}
		return true;
	}

	public static boolean isCreate(Table table, String name) {
		MakerConfig config = MakerConfig.getInstance();
		Map<String, Boolean> map = config.getFunctionMap(table.getName());
		boolean t = map.containsKey(name) ? map.get(name) : false;
		return t;
//		return true;
	}

	public static boolean isCreateMtm(Table table, ManyToMany mtm) {
		MakerConfig config = MakerConfig.getInstance();
		Map<String, Boolean> map = config.getFunctionMap(table.getName());
		for (UniqueIndex index : table.getIndexies()) {
			String name = "getMany" + firstUpper(mtm.getSecondTable().getJavaName()) + "sBy" + uniqueName(index);
			boolean t = map.containsKey(name) ? map.get(name) : false;
			if (!t) {
				name = "getManyJoinAllBy"  + uniqueName(index);
				t = map.containsKey(name) ? map.get(name) : false;
			}
			if (t) return true;
		}
		return false;
//		return true;
	}

	public static boolean isCreateName(Table table, String name) {
		MakerConfig config = MakerConfig.getInstance();
		Map<String, Boolean> map = config.getFunctionMap(table.getName());
		for (UniqueIndex index : table.getIndexies()) {
			String name2 = name  + uniqueName(index);
			boolean t = map.containsKey(name2) ? map.get(name2) : false;
			if (t) return true;
		}
		return false;
	}

	public static boolean isCreateNames(Table table, String... names) {
		MakerConfig config = MakerConfig.getInstance();
		Map<String, Boolean> map = config.getFunctionMap(table.getName());
		for (String name : names) {
			boolean t = map.containsKey(name) ? map.get(name) : false;
			if (t) return true;
		}
		return false;
	}

	public static String uniqueName(UniqueIndex unique) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Column c : unique.getColumns()) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append("And");
			}
			sb.append(firstUpper(c.getProperty()));
		}
		return sb.toString();
	}

	public static String uniqueType(UniqueIndex unique) {
		return unique.getColumns().get(0).getJavaType();
	}

	public static String uniqueParam(UniqueIndex unique) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Column c : unique.getColumns()) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}
			sb.append(c.getJavaType()).append(" ").append(firstLower(c.getProperty()));
		}
		return sb.toString();
	}

	public static String uniqueParamDao(Table table, UniqueIndex unique) {
		StringBuilder sb = new StringBuilder();
		if (unique.getColumns().size() == 1) {
			Column c = unique.getColumns().get(0);
			return sb.append(c.getJavaType()).append(" ").append(firstLower(c.getProperty())).toString();
		}
		boolean isFirst = true;
		for (Column c : unique.getColumns()) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}

			sb.append("@Param(\"").append(firstLower(c.getProperty())).append("\")")
			  .append(c.getJavaType()).append(" ").append(firstLower(c.getProperty()));
		}
		return sb.toString();
	}

	public static String uniqueValue(UniqueIndex unique) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Column c : unique.getColumns()) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}

			sb.append(firstLower(c.getProperty()));
		}
		return sb.toString();
	}

	public static String uniqueParamController(Table table, UniqueIndex unique) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Column c : unique.getColumns()) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}

			sb.append("@PathVariable(\"").append(firstLower(c.getProperty())).append("\")")
					.append(c.getJavaType()).append(" ").append(firstLower(c.getProperty()));
		}
		return sb.toString();
	}

	public static String uniqueParamControllerPath(Table table, UniqueIndex unique) {
		StringBuilder sb = new StringBuilder();
		boolean isFirst = true;
		for (Column c : unique.getColumns()) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(", ");
			}

			sb.append("/{").append(firstLower(c.getProperty())).append("}");
		}
		return sb.toString();
	}

	public static String type(String beanName) {
		String type = beanName;
//		boolean fullPath = MakerConfig.getInstance().getParam().isFullPath();
//		if (fullPath) {
//			if (beanName.contains("Where")) {
//				type = MakerConfig.getInstance().getPojoWherePackage() + "." + beanName;
//			} else {
//				type = MakerConfig.getInstance().getPojoPackage() + "." + beanName;
//			}
//		}
		return type;
	}

	public static String easyuiValiType(Column column) {
		StringBuilder sb = new StringBuilder();
		List<String> types = new ArrayList<String>();
		if (column.isNullable()) {
			types.add("required=true");
		}
		String type = column.getType();
		JdbcType jdbcType = JdbcType.valueOf(type);
		if (jdbcType != null) {
			if (isNumber(column)) {
				types.add("min=0");
			} else if (jdbcType == JdbcType.DATE) {
				types.add("validType=date");
			}  else {
				String length = "[0, " + column.getSize() + "]";
				types.add("validType='length" + length +"'");
			}
		}
		boolean isFirst = true;
		for (String str : types) {
			if (isFirst) {
				isFirst = false;
			} else {
				sb.append(" ");
			}
			sb.append(str);
		}
		return types.size() > 0 ? sb.toString() : null;
	}

	public static Map<String, Boolean> getAllFunction(Table table) {
		Map<String, Boolean> result = new LinkedHashMap<>();
		result.put("insert", true);
		result.put("insertSelective", true);
		boolean isCreate = false;
		for (UniqueIndex ui : table.getIndexies()) {
			isCreate = FreeMakerUtil.canCreate(ui) && !table.isMiddle();
			String uiname = FreeMakerUtil.uniqueName(ui);
			result.put("getBy" + uiname, isCreate);
			result.put("updateBy" + uiname, isCreate);
			result.put("updateNotNullBy" + uiname, isCreate);
			result.put("deleteBy" + uiname, isCreate);
		}

		for (UniqueIndex ui : table.getIndexies()) {
			String uiname = FreeMakerUtil.uniqueName(ui);
			String un = "By" + uiname;
			isCreate = FreeMakerUtil.canCreate(ui) && !table.isMiddle();

			for (ForeignKey fk : table.getForeignKeys()) {
				result.put("getWith" + FreeMakerUtil.firstUpper(fk.getColumn().getProperty()) + un, false);
				result.put("getJoin" + FreeMakerUtil.firstUpper(fk.getColumn().getProperty()) + un, false);
			}
			if (table.getForeignKeys().size() > 1) {
				result.put("getWithAssociates" + un, isCreate);
				result.put("getJoinAssociates" + un, isCreate);
			}
		}

		for (UniqueIndex ui : table.getIndexies()) {
			String uiname = FreeMakerUtil.uniqueName(ui);
			String un = "By" + uiname;
			isCreate = FreeMakerUtil.canCreate(ui) && !table.isMiddle();

			for (ForeignKey fk : table.getMany()) {
				result.put("getJoin" + FreeMakerUtil.firstUpper(fk.getTable().getJavaName()) + un, isCreate);
			}
			if (table.getMany().size() > 1) {
				result.put("getJoinAll" + un, false);
			}
		}

		for (UniqueIndex ui : table.getIndexies()) {
			String uiname = FreeMakerUtil.uniqueName(ui);
			String un = "By" + uiname;
			for (ManyToMany mtm : table.getManyToManys()) {
				boolean isCreate2 = false;
				if (ui.isPrimaryKey() && !table.isMiddle()) {
					isCreate2 = true;
				}
				result.put("getMany" + FreeMakerUtil.firstUpper(mtm.getSecondTable().getJavaName()) + "s" + un, isCreate2);
			}
			if (table.getManyToManys().size() > 1) {
				result.put("getManyJoinAll" + un, false);
			}
		}

		isCreate = !table.isMiddle();
		result.put("find", isCreate);
		if (table.getForeignKeys().size() > 0) {
			result.put("findJoinBy", isCreate);
		}
		return result;
	}

	public static boolean canCreate(UniqueIndex ui) {
		if (ui.isPrimaryKey()) {
			return true;
		} else {
			for(Column c : ui.getColumns()) {
				if (c.isOneToOne()) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean isPrimaryKey(Table table, Column column) {
		if (table.getPrimaryKey().getColumn() != null && column.getProperty().equals(table.getPrimaryKey().getColumn().getProperty())) {
			return true;
		}
		return false;
	}

	public static String selectSql(Table table) {
		String columns = table.getColumnList().stream().map(c->selectColumn(c)).collect(Collectors.joining(", "));
		return "SELECT " + columns + " FROM " + table.getName();
	}

	private static String selectColumn(Column column) {
		if (column.getName().contains("_")) {
			return column.getName() + " as " + column.getProperty();
		}
		return column.getName();
	}

	public static String insertSql(Table table) {
		String columns = table.getColumnList().stream().filter(c->!table.getPrimaryKey().getColumn().equals(c)).map(e->e.getName()).collect(Collectors.joining(", "));
		String params = table.getColumnList().stream().filter(c->!table.getPrimaryKey().getColumn().equals(c)).map(e->"?").collect(Collectors.joining(", "));
		return "INSERT INTO " + table.getName() + "(" + columns + ") values(" + params + ")";
	}

	public static String updateSql(Table table) {
		String columns = table.getColumnList().stream().filter(c->!table.getPrimaryKey().getColumn().equals(c)).map(e->e.getName() + "=?").collect(Collectors.joining(", "));
		return "UPDATE " + table.getName() + " SET " + columns + " WHERE " + table.getPrimaryKey().getColumn().getProperty() + "=?";
	}

	public static String whereParamName(Table table) {
		String whereParamName = table.getColumnList().stream().filter(c->!table.getPrimaryKey().getColumn().equals(c) || !isDate(c)).map(e-> "\"" + e.getProperty() + "\"").collect(Collectors.joining(","));
		return whereParamName;
	}

	public static String insertParamName(Table table) {
		String insertParamName = table.getColumnList().stream().filter(c->!table.getPrimaryKey().getColumn().equals(c)).map(e-> "\"" + e.getProperty() + "\"").collect(Collectors.joining(","));
		return insertParamName;
	}

	public static String dartType(Column column) {
		return "String";
	}

	public static String dartName(String name) {
		StringBuffer sb = new StringBuffer();
		char[] chars = name.toCharArray();
		for (int i=0; i<chars.length; i++) {
			if (Character.isUpperCase(chars[i])) {
				char c = Character.toLowerCase(chars[i]);
				if (i != 0) {
					sb.append("-");
				}
				sb.append(c);
			} else {
				sb.append(chars[i]);
			}
		}
		return sb.toString();
	}


	public static String dartBeanParams(Table table) {
		List<Column> columns = table.getColumnList();
		List<Column> columnList = columns.stream().filter(column -> column.isCreate()).collect(Collectors.toList());
		return columnList.stream().map(column -> "this." + column.getProperty()).collect(Collectors.joining(" ,"));
	}

	public static void main(String[] args) {
		Integer id = 12;
		if (id.getClass().isPrimitive() || (Number.class.isAssignableFrom(id.getClass()))) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}
	}
}
